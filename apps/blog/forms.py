from django import forms
from apps.blog.models import Post, Category


class PostModelForm(forms.ModelForm):
    category = forms.ModelChoiceField(
        queryset=Category.objects.all().order_by('name'),
        widget=forms.Select(attrs={'class': 'form-control'})
    )

    class Meta:
        model = Post
        fields = [
            "title",
            "text",
            "category"
        ]
        widgets = {
            "text": forms.Textarea(
                attrs={
                    "placeholder": "New Text",
                    "class": "form-control"
                }
            ),
            "title": forms.TextInput(
                attrs={
                    "placeholder": "Title",
                    "class": "form-control"
                }
            )
        }


class CategoryModelForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = [
            "name"
        ]
        widgets = {
            "name": forms.TextInput(
                attrs={
                    "placeholder": "Name",
                    "class": "form-control"
                }
            )
        }




