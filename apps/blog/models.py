from django.conf import settings
from django.core.validators import MaxLengthValidator
from django.db import models



class Post(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    title = models.CharField(max_length=60)
    text = models.TextField(
        validators=[MaxLengthValidator(2000)]
    )
    pub_date = models.DateTimeField(auto_now_add=True)
    category = models.ForeignKey(
        verbose_name='category',
        to='Category')

    def __unicode__(self):
        return "{}".format(self.title)

    def get_absolute_url(self):
        return "/posts/{}".format(self.pk)

class Category(models.Model):
    name = models.CharField(max_length=30)
    # slug = models.SlugField(null=True)

    class Meta:
        # db_table = 'category'
        # ordering = ['name']
        verbose_name_plural = "Categories"

    def __unicode__(self):
        return "{}".format(self.name)

    def get_absolute_url(self):
        return "/posts/category/{}".format(self.pk)

