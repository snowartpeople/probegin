from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout
from django.http import HttpResponseRedirect

from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView, FormView, View

from probegin.mixins import SubmitBtnMixin, FormTitleMixin

from apps.blog.models import Post, Category
from .forms import PostModelForm, CategoryModelForm


class PostCreateView(SubmitBtnMixin, FormTitleMixin, CreateView):
    model = Post
    template_name = "post_form_create_update.html"
    form_class = PostModelForm
    submit_btn = "Add Post"
    form_title = "Add new post"

    def form_valid(self, form):
        user = self.request.user
        form.instance.user = user
        return super(PostCreateView, self).form_valid(form)


class PostUpdateView(SubmitBtnMixin, FormTitleMixin, UpdateView):
    model = Post
    template_name = "post_form_create_update.html"
    form_class = PostModelForm
    success_url = "/posts/"
    submit_btn = "Update Post"
    form_title = "Edit post"
    pk_url_kwarg = 'pk'


class PostDetailView(DetailView):
    model = Post


class PostDeleteView(DeleteView):
    model = Post
    success_url = "/posts/"
    pk_url_kwarg = 'pk'
    template_name = 'confirm_delete.html'


class PostListView(ListView):
    model = Post

    def get_queryset(self):
        qs = super(PostListView, self).get_queryset().order_by('-pub_date')[:5]
        return qs


class CategoryListView(ListView):
    model = Category


class CategoryCreateView(SubmitBtnMixin, CreateView):
    model = Category
    template_name = "category_form.html"
    form_class = CategoryModelForm
    success_url = "/categories/"
    submit_btn = "Add Category"

    def form_valid(self, form):
        user = self.request.user
        form.instance.user = user
        return super(CategoryCreateView, self).form_valid(form)

class CategoryDeleteView(DeleteView):
    model = Category
    success_url = "/categories/"
    pk_url_kwarg = 'pk'
    template_name = 'confirm_delete.html'


class LoginFormView(FormView):
    form_class = AuthenticationForm
    template_name = "login.html"
    success_url = "/"

    def form_valid(self, form):
        self.user = form.get_user()
        login(self.request, self.user)
        return super(LoginFormView, self).form_valid(form)


class LogoutView(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect("/")


def filtered_posts(request, id):
    category = Category.objects.get(id=id)
    posts = category.post_set.all()
    return render(request, 'blog/category_filtered_posts.html', {'posts': posts, 'category': category})
