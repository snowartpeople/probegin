from django.contrib import admin

from apps.blog.models import Post, Category


class PostAdmin(admin.ModelAdmin):
    list_display = ["__unicode__"]
    list_filter = ["pub_date"]

    class Meta:
        model = Post

admin.site.register(Post, PostAdmin)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ["__unicode__"]

    class Meta:
        model = Category

admin.site.register(Category, CategoryAdmin)

