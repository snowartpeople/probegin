from django.conf.urls import include, url
from django.contrib import admin

from apps.blog.views import PostCreateView, PostDetailView, PostListView, PostUpdateView, filtered_posts, \
    CategoryListView, CategoryCreateView, PostDeleteView, CategoryDeleteView, LoginFormView, LogoutView


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', PostListView.as_view(), name='post_list_view'),

    # posts
    url(r'^posts/$', PostListView.as_view(), name='post_list_view'),
    url(r'^posts/add/$', PostCreateView.as_view(), name='post_create_view'),
    url(r'^posts/delete/(?P<pk>\d+)$', PostDeleteView.as_view(), name='post_delete_view'),
    url(r'^posts/(?P<pk>\d+)/$', PostDetailView.as_view(), name='post_detail_view'),
    url(r'^posts/update/(?P<pk>\d+)/$', PostUpdateView.as_view(), name='post_update_view'),
    url(r'^posts/category/(?P<id>\d+)$', filtered_posts),

    # categories
    url(r'^categories/$', CategoryListView.as_view(), name='category_list_view'),
    url(r'^categories/add/$', CategoryCreateView.as_view(), name='category_create_view'),
    url(r'^categories/delete/(?P<pk>\d+)$', CategoryDeleteView.as_view(), name='category_delete_view'),

    # authentication
    # url(r'^login$', TemplateView.as_view(template_name='login.html'), name='login'),
    url(r'^login/$', LoginFormView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
]
