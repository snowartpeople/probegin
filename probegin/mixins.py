class SubmitBtnMixin(object):
    submit_btn = None

    def get_context_data(self, *args, **kwargs):
        context = super(SubmitBtnMixin, self).get_context_data(*args, **kwargs)
        context["submit_btn"] = self.submit_btn
        return context


class FormTitleMixin(object):
    form_title = None

    def get_context_data(self, *args, **kwargs):
        context = super(FormTitleMixin, self).get_context_data(*args, **kwargs)
        context["form_title"] = self.form_title
        return context
